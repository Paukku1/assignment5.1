"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculator = void 0;
function calculator(operator, num1, num2) {
    if (operator === '+') {
        return num1 + num2;
    }
    else if (operator === '-') {
        return num1 - num2;
    }
    else if (operator === '*') {
        return num1 * num2;
    }
    else if (operator === '/') {
        if (num1 === 0 || num2 === 0) {
            return 'Can not do that!';
        }
        return num1 / num2;
    }
    else {
        return 'Can not do that!';
    }
}
exports.calculator = calculator;
console.log(calculator('+', 1, 2));
